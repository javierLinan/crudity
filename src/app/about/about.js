angular.module('ngCrudity.about', [
  'ui.router',
  'ui.bootstrap'
])

.config(function config($stateProvider) {
  $stateProvider.state('about', {
    url: '/about',
    views: {
      "": {
        controller: 'AboutCtrl',
        templateUrl: 'about/about.tpl.html'
      }
    },
    data:{
      pageTitle: 'About' 
    }
  });
})

.controller('AboutCtrl', function AboutCtrl($scope) {
 
});