angular.module( 'ngCrudity.contacts', [
  'ui.router'
  ])

.config(function config($stateProvider) {
  $stateProvider
  
    // Contacts

    .state('contacts', {
      abstract: true,  
      url: '/contacts',
      controller: 'ContactsCtrl',
      templateUrl: 'contacts/contacts.tpl.html',
      data:{ pageTitle: 'Contacts' }
    })

    // Contacts -> List

    .state('contacts.list', {
      url: '',
      templateUrl: 'contacts/contacts.list.tpl.html'
    })

    // Contacts -> Add

    .state('contacts.add', {
      url: '/add',
      templateUrl: 'contacts/contacts.add.tpl.html',
      controller: 'ContactsAddCtrl'
    })  

    // Contacts -> Detail

    .state('contacts.detail', {
      url: '/{contactId:[0-9]{1,4}}',
      templateUrl: 'contacts/contacts.detail.tpl.html',
      controller: 'ContactsDetailCtrl'
    })    

    // Contacts -> Detail -> Update
    .state('contacts.detail.update', {
      templateUrl: 'contacts/contacts.detail.update.tpl.html',
      controller: 'ContactsDetailUpdateCtrl'
    })    

    // Contacts -> Detail -> Delete

    .state('contacts.detail.delete', {
      templateUrl: 'contacts/contacts.detail.delete.tpl.html',
      controller: 'ContactsDetailDeleteCtrl'
    });  
  })

.controller( 'ContactsCtrl', function ContactsCtrl($scope, Contact) {

  $scope.contacts = Contact.getContacts();

  $scope.$on('contacts.update', function(event) {
    $scope.contacts = Contact.getContacts();
  });  
})

.controller( 'ContactsAddCtrl', function ContactsAddCtrl() {
})

.controller( 'ContactsDetailCtrl', function ContactsDetailCtrl($scope, $stateParams, Contact) {

  $scope.contact = Contact.getContact($stateParams.contactId);

})

.controller( 'ContactsDetailUpdateCtrl', function ContactsDetailUpdateCtrl($scope, $stateParams, Contact) {

  $scope.contact = Contact.getContact($stateParams.contactId);
  $scope.updateContact = angular.copy($scope.contact);

})

.controller( 'ContactsDetailDeleteCtrl', function ContactsDetailDeleteCtrl($scope, $stateParams, Contact) {

  $scope.contact = Contact.getContact($stateParams.contactId);

})

.directive('ngAllcontactsbutton', function($state, $stateParams, Contact) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind( 'click', function() {
        // Contact.setMockContacts();
        scope.query = '';
        $state.go('contacts.list', $stateParams);
      });        
    }
  };
})

.directive('ngConfirmupdatebutton', function($state, $stateParams, Contact) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind( 'click', function() {
        scope.updateContact.picture = scope.imageSrc ? scope.imageSrc : null;
        Contact.updateContact(scope.updateContact);
        angular.copy(scope.updateContact, scope.contact);
        scope.udpateContact = {};
        $state.go('^', $stateParams);
      });        
    }
  };
})

.directive('ngConfirmdeletebutton', function($state, $stateParams, Contact) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind( 'click', function() {
        Contact.removeContact(scope.contact);
        scope.contact = {};
        $state.go('contacts.list', $stateParams);
      });        
    }
  };
})

.directive('ngConfirmaddbutton', function($state, $stateParams, Contact) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind( 'click', function() {
        scope.newContact.picture = scope.imageSrc ? scope.imageSrc : null;
        Contact.addContact(scope.newContact);
        scope.newContact = {};
        $state.go('contacts.list', $stateParams);
      });        
    }
  };
})

.directive('ngReadimage', function($rootScope) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind('change', function(event) {
        var imageFile = event.currentTarget.files[0],
          reader;

        if(imageFile) {
          reader = new FileReader();

          reader.onload = function(e) {
            scope.imageSrc = e.target.result;
            $rootScope.$broadcast('picture.update', e.target.result);
          };

          reader.readAsDataURL(imageFile);
        }
      });        
    }
  };
})

.directive('ngUpdatepicture', function() {
  return {  
    restrict: 'A',
    link: function(scope, element, attrs) {

      scope.$on('picture.update', function(event, imageSrc) {

        var img = document.createElement('img'),
          oldImg = element.find('img');

        img.src = imageSrc;

        if(oldImg.length > 0) {
          oldImg.remove();
        }

        element.append(img);
      });        
    }
  };
}); 