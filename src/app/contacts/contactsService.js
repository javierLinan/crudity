angular.module('ngCrudity.contacts.service', [
  'LocalStorageModule'
  ])

.service('Contact', function(localStorageService, $rootScope){
  var service = {},
    contacts;

  //Private methods and attributes

  function setMockContacts() {
    mockContacts = [
      {
        'id': '1',
        'name': 'Alice',
        'surname': 'Morgan',
        'email': 'alice@alice.com',
        'address': 'Street of Alice, 6',
        'picture': null
      },
      {
        'id': '2',
        'name': 'Bob',
        'surname': 'Freeman',
        'email': 'bob@bob.com',
        'address': 'Street of Bob, 6',
        'picture': null        
      },
      {
        'id': '3',
        'name': 'Eve',
        'surname': 'Fisherman',
        'email': 'eve@eve.com',
        'address': 'Street of Bob, 6',
        'picture': null        
      }
    ];

    localStorageService.set('contacts', mockContacts);
  }

  function initContacts() {
    var contactsStored = localStorageService.get('contacts');

    if(!contactsStored) {
      contactsStored = [];
    }    

    return contactsStored;
  }

  function findObjectInArr(key, value, arr) {

    var remainingArr;

    if(arr[0][key] == value) {
      return arr[0];
    } else {
      remainingArr = arr.slice(1);

      if(remainingArr.length > 0) {
        return findObjectInArr(key, value, remainingArr);
      } else {
        return null;
      }
    }
  }

  function updateObjectInArr(key, object, arrUpdated, arr) {
    var currentObject = arr[0],
      remainingArr = arr.slice(1);

    if(currentObject[key] == object[key]) {
      arrUpdated.push(object);
    } else {
      arrUpdated.push(currentObject);
    }

    if(remainingArr.length > 0) {
      return updateObjectInArr(key, object, arrUpdated, remainingArr);
    } else {
      return arrUpdated;
    }
  }

  function removeObjectFromArr(key, object, arrUpdated, arr) {
    var currentObject = arr[0],
      remainingArr = arr.slice(1);

    if(currentObject[key] != object[key]) {
      arrUpdated.push(currentObject);
    } 

    if(remainingArr.length > 0) {
      return removeObjectFromArr(key, object, arrUpdated, remainingArr);
    } else {
      return arrUpdated;
    }
  }

  contacts = initContacts();

  //Public methods and attributes

  service.setMockContacts = function() {
    setMockContacts();
    contacts = initContacts();
    $rootScope.$broadcast('contacts.update');
  };

  service.getContacts = function() {
    return contacts;
  };

  service.getContact = function(id) {
    return findObjectInArr('id', id, contacts);
  };

  service.addContact = function(contact) {

    if(contacts.length > 0) {

      // if(findObjectInArr('email', contact.email, contacts)) {
      //   throw new Error('Already exists a contact with the email ' + contact.email + '.');
      // }

      contact.id = parseInt(contacts[contacts.length - 1].id, 10) + 1;

    } else {
      contact.id = '1';
    }

    contacts.push(contact);

    localStorageService.set('contacts', contacts);

    $rootScope.$broadcast('contacts.update');
  };

  service.updateContact = function(contact) {

    // if(findObjectInArr('email', contact.email, contacts)) {
    //   throw new Error('Already exists a contact with the email ' + contact.email + '.');
    // }

    contacts = updateObjectInArr('id', contact, [], contacts);

    localStorageService.set('contacts', contacts);

    $rootScope.$broadcast('contacts.update');
  };

  service.removeContact = function(contact) {
    contacts = removeObjectFromArr('id', contact, [], contacts);

    localStorageService.set('contacts', contacts);

    $rootScope.$broadcast('contacts.update');
  };

  return service;

});