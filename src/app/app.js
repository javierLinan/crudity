angular.module('ngCrudity', [
  'templates-app',
  'templates-common',
  'ngCrudity.home',
  'ngCrudity.about',
  'ngCrudity.contacts',
  'ngCrudity.contacts.service',
  'ui.router',
  'LocalStorageModule'
])

.config(function myAppConfig ($stateProvider, $urlRouterProvider, localStorageServiceProvider) {
  $urlRouterProvider.otherwise('/home');
  localStorageServiceProvider.setPrefix('crudity');
})

.run( function run($rootScope, $state, $stateParams) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
})

.controller('AppCtrl', function AppCtrl($scope, $location) {

  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    if(angular.isDefined(toState.data.pageTitle)) {
      $scope.pageTitle = toState.data.pageTitle + ' | ngCrudity' ;
    }
  });
})

;

