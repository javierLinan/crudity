## CRUDity ##

This is a simple CRUD application developed with AngularJS with the purpose of, through the practice, understand the AngularJS binding, model view, directives, etc.

## Features ##

We are working on this features: 

* Creation / listing / viewing / editing / deleting / searching for contacts using basic attributes (name + surname + ...) stored in the browser (or other localStorage).
* Upload a photo of a contact.
* Tagging contacts and search using tags.

## How do I get set up? ##

Install Node.js and then:


```
#!javascript

$ git clone https://javierLinan@bitbucket.org/javierLinan/crudity.git
$ cd crudity
$ sudo npm -g install grunt-cli karma bower
$ npm install
$ bower install
$ grunt watch
```